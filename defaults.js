const isObject = require("./isObject");
const keys = require("./keys");

function defaults(object, defaultProps) {
  if (!isObject(object) && !isObject(defaultProps)) {
    console.log("please enter the object details");
  } else {
    let _keys = keys(defaultProps);
    for (let index = 0; index < _keys.length; index++) {
      if (object[_keys[index]] === undefined) {
        object[_keys[index]] = defaultProps[_keys[index]];
      }
    }
  }
  return object;
}

module.exports = defaults;
