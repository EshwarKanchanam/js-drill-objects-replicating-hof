const isObject = require("./isObject");
const keys = require("./keys");

function mapObject(object, callback) {
  let mappedObject = {};

  if (!isObject(object)) {
    console.log("please enter the array details");
  } else {
    keys(object).forEach((key) => {
      mappedObject = { ...mappedObject, [key]: callback(object[key], key) };
    });
  }

  return mappedObject;
}

module.exports = mapObject;
