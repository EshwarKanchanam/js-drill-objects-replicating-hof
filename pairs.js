const isObject = require("./isObject");

function pairs(object) {
  let pairs = [];

  if (!isObject(object)) {
    console.log("please enter the object");
  } else {
    for (let key in object) {
      pairs.push([key, object[key]]);
    }
  }
  return pairs;
}

module.exports = pairs;
