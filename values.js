const isObject = require("./isObject");
const keys = require("./keys");

function values(object) {
  let values = [];

  if (!isObject(object)) {
    console.log("please enter the object");
  } else {
    let currKeys = keys(object);

    values = currKeys
      .filter((key) => typeof object[key] !== "function")
      .map((key) => object[key]);
  }
  return values;
}

module.exports = values;
