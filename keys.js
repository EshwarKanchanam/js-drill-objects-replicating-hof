const isObject = require("./isObject");

function keys(object) {
  let keys = [];
  if (!isObject(object)) {
    console.log("please enter the object");
  } else {
    for (let key in object) {
      keys.push(key);
    }
  }
  return keys;
}

module.exports = keys;
