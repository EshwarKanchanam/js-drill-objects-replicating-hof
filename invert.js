const keys = require("./keys");

function invert(object) {
  let invertedObject = {};
  let _keys = keys(object);

  for (let index = 0; index < _keys.length; index++) {
    invertedObject[object[_keys[index]]] = _keys[index];
  }
  return invertedObject;
}

module.exports = invert;
